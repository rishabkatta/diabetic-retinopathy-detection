import os
import pickle
import time

import numpy as np
import pandas as pd
from keras.applications.vgg16 import preprocess_input
from keras.applications.vgg16 import VGG16
from keras.layers import Dense, Dropout
from keras.layers import Input
from keras.models import Model
from keras.preprocessing import image
from keras.utils import np_utils
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle


def train_vgg16(train_img_folder_path, train_labels_file_path, trained_model_file_path, trained_hist_file_path):
    img_data_list = []
    img_names_list = os.listdir(train_img_folder_path)
    img_names_list = img_names_list[0:100]  # remove later

    for img in img_names_list:
        img_path = train_img_folder_path + img
        img = image.load_img(img_path, target_size=(224, 224))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x)
        img_data_list.append(x)

    img_data = np.array(img_data_list)
    img_data = np.rollaxis(img_data, 1, 0)
    img_data = img_data[0]
    num_classes = 5
    num_of_samples = img_data.shape[0]
    labels = np.ones((num_of_samples,), dtype='int64')
    data = pd.read_csv(train_labels_file_path)
    data = np.array(data)
    r, c = data.shape
    mp = {}
    for i in range(0, r):
        mp[str(str(data[i][0]) + '.jpeg')] = data[i][1]

    i = 0
    dirs = os.listdir(train_img_folder_path)
    for img in img_names_list:
        if not (os.path.isfile(train_img_folder_path + img)) or (not (img.endswith('.jpeg'))):
            continue
        labels[i] = (mp.get(img))
        i = i + 1
    names = ['NoDR', 'EarlyDR', 'ModerateDR', 'SevereDR', 'NPDR']
    Y = np_utils.to_categorical(labels, num_classes)

    # Shuffle the dataset
    x, y = shuffle(img_data, Y, random_state=2)
    # Split the dataset
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2)

    image_input = Input(shape=(224, 224, 3))
    vgg16_model = VGG16(input_tensor=image_input, include_top=True, weights='imagenet')
    vgg16_model.summary()

    last_layer = vgg16_model.output
    x = Dense(512, activation='relu', name='fc-1')(last_layer)
    x = Dropout(0.5)(x)
    x = Dense(256, activation='relu', name='fc-2')(x)
    x = Dropout(0.5)(x)
    # a softmax layer for 5 classes
    out = Dense(num_classes, activation='softmax', name='output_layer')(x)

    # train test on VGG16
    custom_vgg16_model = Model(inputs=image_input, outputs=out)
    custom_vgg16_model.summary()

    for layer in custom_vgg16_model.layers[:-6]:
        layer.trainable = False

    custom_vgg16_model.layers[-1].trainable

    custom_vgg16_model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    t = time.time()
    vgg16 = custom_vgg16_model.fit(X_train, y_train, batch_size=32, epochs=20, verbose=1,
                                   validation_data=(X_test, y_test))
    print('Training time: %s' % (t - time.time()))

    # model persistance
    with open(trained_model_file_path, 'wb') as file:
        pickle.dump(custom_vgg16_model, file)

    with open(trained_hist_file_path, 'wb') as file:
        pickle.dump(vgg16.history, file)

    (loss, accuracy) = custom_vgg16_model.evaluate(X_test, y_test, batch_size=10, verbose=1)
    print("[INFO] loss={:.4f}, accuracy: {:.4f}%".format(loss, accuracy * 100))


# train with normal data
train_vgg16(train_img_folder_path='../processed_299_299/',
            train_labels_file_path='../trainLabels.csv',
            trained_model_file_path='../trained_models/trained_vgg16.pkl',
            trained_hist_file_path='../trained_hist/vgg16.history')